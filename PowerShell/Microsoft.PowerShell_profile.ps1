Function Prompt
{
	"$ "
}

Set-Alias -Name ll -Value Get-ChildItem
Import-Module ActiveDirectory
Import-Module Pester

<#
.Synopsis
   Searches files.
.DESCRIPTION
   Finds files that contains search string.
.EXAMPLE
   Find-InFiles -FindInDir "C:\Temp" -SearchString "Test"
#>
Function Check-LockedAccounts
{
	Begin{}
	Process
	{
		Search-ADAccount -LockedOut
	}
	End{}
}

Function Find-InFiles
{
    [CmdletBinding()]
    [OutputType([int])]
    Param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)] 
        [string] $FindInDir,

        [Parameter(Mandatory=$true,
                   Position=1)] 
        [string] $SearchString,

        [Parameter(Mandatory=$false, Position=2)]
        [string] $FileFilter = "*.*"

    )

    Begin
    {
    }
    Process
    {
		Get-ChildItem -File -Filter $FileFilter -Path $FindInDir -Recurse -Force -ErrorAction Ignore | Where-Object { Select-String $SearchString $_ -Quiet }
    }
    End
    {
    }
}

Function Get-SamAccountName
{
	[CmdletBinding()]
    [OutputType([string])]
    Param
    (
        [Parameter(Mandatory=$True, ValueFromPipelineByPropertyName=$true, Position=0)] 
        [string] $FullName
    )

    Begin
    {
    }
    Process
    {
		$user = Get-ADUser -filter {DisplayName -eq $FullName} | Select Name, SamAccountName
		Return $user
    }
    End
    {
    }
}